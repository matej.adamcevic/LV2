# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 20:56:09 2015

@author: matej
"""

import urllib 
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt

url_adresa = 'http://en.wikipedia.org/wiki/Demographics_of_Croatia'
html = urllib.request.urlopen(url_adresa).read()
soup = BeautifulSoup(html)

table=soup.find("table", {"class":"wikitable"})
godine=[]
vrijednosti=[]
a = ""
for row in table.findAll("tr"):         #tr-oznaka za red u tablici
    cells=row.findAll("td")         #td-oznaka za pojedinu celiju
    if(len(cells)==5):              #svi redovi trazene tablice imaju 5 celija
        god=(cells[0].find(text=True))  #prva celija sadrzi godinu
        vr=(cells[1].find(text=True))   #druga sadrzi broj stanovnika te godine
        for j in range (0, len(vr)):       #brojevi su zapisani sa zarezima: npr 4,252,236
            if(vr[j] is not ','):        #python ih ne moze direktno prebaciti u float tip
                a+=vr[j]                #sve znamenke se nizu u string i na kraju prebacuju u float
        godine.append(god)              #lista koja sprema vrijednosti godina
        vrijednosti.append(float(a))    #lista koja sprema broj stanovnika
        a=""                            #string a se resetira za sljedeci broj stan.
#print(godine, vrijednosti)
for i in range (len(godine)):
    plt.plot(godine[i], vrijednosti[i], 'b .')  