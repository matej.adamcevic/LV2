# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 00:15:39 2015

@author: matej
"""
import urllib 
from bs4 import BeautifulSoup

url_adresa="http://"
url_adresa+=input('Unesite zeljenu adresu: ');

html = urllib.request.urlopen(url_adresa).read()
soup = BeautifulSoup(html)
tags = soup.find_all("a")
for tag in tags:
    adr=str(tag.get('href', None))      #pretvaranje u string radi lakse obrade
    if(not adr.startswith("/")):        #adrese koje ne treba prikazati
        if(not adr.startswith(".")):
            if(not adr.startswith("#")):
               # if(adr.rfind('.')):
                #   domain=adr.split('.')[0]
                 #  print("\n*************\n",domain,"\n***********\n")
                  # dom="www."+domain
                if(not adr.__contains__("www.etfos")):
                    if (len(adr)>4):
                        print("Naslov: ", tag.find(text=True))
                        print ('Adresa:', tag.get('href', None),'\n****\n') 
                        print(len(adr))
                        
#s="www.etfos.unios.hr.b"
#s.rfind('.')
#print(s.rfind('.'))
