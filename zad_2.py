# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 16:04:56 2015

@author: matej
"""
import numpy as np
import matplotlib.pyplot as plt
import random


rang=100
niz=np.zeros(rang)
s = np.random.randint(0,2,rang)
i=0
br1=0
br0=0
while i<len(s):
    if(s[i]==1):
        niz[i]=random.gauss(180,7)
        br1+=1
    else:
        niz[i]=random.gauss(167,7)
        br0+=1
    i+=1

print("Prosjecna visina zena: ",(sum(niz)-np.dot(s, niz))/br0, "cm")
print("Prosjecna visina muskaraca: ",np.dot(s, niz)/br1, "cm")
#plt.plot(niz, np.zeros(rang))
for k in range (0,rang):
    if(s[k]==1):
        plt.plot(niz[k],0,'b .')
    else:
        plt.plot(niz[k],0,'r .')
plt.show()