# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:41:06 2015

@author: matej
"""

import numpy as np
import matplotlib.pyplot as plt

data=np.genfromtxt('mtcars.csv', delimiter=',', dtype=str) #razdvajanje na zarezima
row1=data[1:, 1]        #drugi stupac-potrosnja u mpg
row2=data[1:, 4]        #peti stupac-konjska snaga
wt=data[1:, 6]          #sedmi stupac-tezina

fig, ax=plt.subplots()
ax.scatter(row1, row2, 70, 'g')
for i,txt in enumerate(wt):     #tekst uz podatke
    ax.annotate(txt, (row1[i],row2[i] ) )
    